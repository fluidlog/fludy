# fludy

# Presentation
Create and visualize network graphs

# Technos
HTML5/JS/D3JS

# Dependances

* https://gitlab.com/fluidlog/fluidgraph.git
* https://github.com/Open-Initiative/LDP-framework.git

Please read documentation on the wiki : https://gitlab.com/fluidlog/fludy/wikis/home
